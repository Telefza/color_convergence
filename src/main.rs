use nannou::prelude::*;
use rand::Rng;

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

struct Model {
    my_rect: Rectangle,
    my_attractor: Attractor,
}

impl Model {
    fn upadte_attractor(&mut self) {
        // randomly changes the attractor color every time the rectangel converged.
        let (hue_diff, sat_diff, light_diff) =
            color_diff(self.my_rect.color, self.my_attractor.color);
        if (hue_diff).abs() + sat_diff.abs() + light_diff.abs() < 0.1
            && self.my_rect.color_direction.0.abs()
                + self.my_rect.color_direction.1.abs()
                + self.my_rect.color_direction.2.abs()
                < 0.0001
        {
            // update attractor to random color
            self.my_attractor.color = hsl(
                rand::thread_rng().gen_range(0.0..1.0),
                rand::thread_rng().gen_range(0.4..0.7),
                rand::thread_rng().gen_range(0.4..0.7),
            );
            println!(
                "new color hue {}",
                self.my_attractor.color.hue.to_positive_degrees() / 360.0
            );
        }
    }
}

struct Attractor {
    color: Hsl,
    strength: f32,
}

fn color_diff(color_end: Hsl, color_start: Hsl) -> (f32, f32, f32) {
    // Input: two colors
    // Output: the difference in color
    // takes into acout the fact that hue is a circular type.
    // https://docs.rs/nannou/0.17.1/nannou/color/struct.RgbHue.html
    // =============================================================
    // break colors into components
    let (hue_end, sat_end, light_end) = color_end.into_components();
    let hue_end_f32 = hue_end.to_positive_degrees() / 360.0;
    let (hue_start, sat_start, light_start) = color_start.into_components();
    let hue_start_f32 = hue_start.to_positive_degrees() / 360.0;

    let mut hue_direction = hue_end_f32 - hue_start_f32;
    // adjust hue_direciton for circularity.
    if hue_direction < -0.5
    // moving back but so much that you should go forward
    {
        hue_direction = 1.0 + hue_direction;
    }
    if hue_direction > 0.5
    // moving forward but so much that you should go backwards
    {
        hue_direction = hue_direction - 1.0;
    }
    return (hue_direction, sat_end - sat_start, light_end - light_start);
}

struct Rectangle {
    color: Hsl,
    color_direction: (f32, f32, f32),
    drag: f32,
}

impl Rectangle {
    fn update_color(&mut self) {
        // split into components
        let (my_hue, my_sat, my_light) = self.color.into_components();
        let my_hue_f32 = my_hue.to_positive_degrees() / 360.0;
        // add the components to the
        self.color = hsl(
            my_hue_f32 + self.color_direction.0,
            my_sat + self.color_direction.1,
            my_light + self.color_direction.2,
        );
    }

    fn update_color_direction(&mut self, attract: &Attractor) {
        let (hue_direction, sat_direction, light_direction) = color_diff(attract.color, self.color);
        self.color_direction = (
            self.color_direction.0 + attract.strength * hue_direction,
            self.color_direction.1 + attract.strength * sat_direction,
            self.color_direction.2 + attract.strength * light_direction,
        );
    }

    fn dampen_color_direction(&mut self) {
        // break into components
        let (hue, sat, light) = self.color_direction;
        self.color_direction = (hue * self.drag, sat * self.drag, light * self.drag);
    }
}

fn model(_app: &App) -> Model {
    Model {
        my_rect: Rectangle {
            color: hsl(1.0, 0.5, 0.5),
            color_direction: (0.0, 0.0, 0.0),
            drag: 0.995,
        },
        my_attractor: Attractor {
            color: hsl(0.33, 0.5, 0.5),
            strength: 0.0018,
        },
    }
}

fn update(_app: &App, model: &mut Model, _update: Update) {
    model.my_rect.update_color_direction(&model.my_attractor);
    model.my_rect.dampen_color_direction();
    model.my_rect.update_color();
    model.upadte_attractor();
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    draw.background().color(BLACK);
    draw.rect().color(model.my_rect.color);
    draw.to_frame(app, &frame).unwrap();
    // to create the png files. takes forever to actually do.
    // let file_path = format!("capture/frame-{:04}.png", frame.nth());
    // app.main_window().capture_frame(file_path);
    //fmpeg command
    // ffmpeg -framerate 60 -i capture/frame-%04d.png -pix_fmt yuv420p -vcodec libx264 -preset veryslow -tune animation -crf 18 -r 60 out.mp4
}
